import React from 'react'
import { styled } from '@mui/material/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton } from '@mui/material';
import { deleteItem } from '../api/cart';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.body}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
'&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
},
// hide last border
'&:last-child td, &:last-child th': {
    border: 0,
},
}));

export const OrderItemRow = ({item, orderId, setOrder}) => {
    const {id, product, quantity} = item;
    const {name, prize} = product;


  return (
    <StyledTableRow key={id}>
        <StyledTableCell align="center">{name}</StyledTableCell>
        <StyledTableCell align="center">{quantity}</StyledTableCell>
        <StyledTableCell align="center">{(prize * quantity).toFixed(2)}</StyledTableCell>
        <StyledTableCell align="center">
          <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="error"
              onClick={() => deleteItemProduct(id)}
          >
            <DeleteIcon />
          </IconButton>
        </StyledTableCell>
    </StyledTableRow>
  )

  function deleteItemProduct(itemId){
    console.log(itemId, orderId)
    deleteItem(orderId, itemId).then(order => setOrder(order))
  }
}
