import { environment } from "../config/environment";

export function getProducts(){
    return fetch(`${environment.baseUrl}/products`)
        .then(res => res.json())
}

export async function getProduct(productId){
    const res = await fetch(`${environment.baseUrl}/products/${productId}`);
    return await res.json();
}

export function saveProduct(product) {
    return fetch(`${environment.baseUrl}/products`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "name": product.name,
            "description": product.description,
            "prize": product.prize
        })
    })
}

export function deleteProduct(productId) {
    return fetch(`${environment.baseUrl}/products/${productId}`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    })
}