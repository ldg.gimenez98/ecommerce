import { environment } from "../config/environment";

export function getCarts(){
    return fetch(`${environment.baseUrl}/carts`)
        .then(res => res.json())
}

export function getCart(cartId){
    return fetch(`${environment.baseUrl}/carts/${cartId}`)
        .then(res => res.json())
}

export function getCartsByCustomer(customerId){
    return fetch(`${environment.baseUrl}/carts/customer/${customerId}`)
        .then(res => res.json())
}


export async function createCart(date, customerId) {
    const res = await fetch(`${environment.baseUrl}/carts/new/${customerId}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            date
        )
    });
    return await res.json();
}

export async function addProductToOrder(cartId, productId) {
    const res = await fetch(`${environment.baseUrl}/carts/${cartId}/add/${productId}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    });

    return await res.json();
}

export function updateDate(cartId, date) {
    return fetch(`${environment.baseUrl}/carts/${cartId}`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(date)
    }).then(res => res.json())
}

export function updateCart(cart) {
    return fetch(`${environment.baseUrl}/carts`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(cart)
    }).then(res => res.json())
}

export function deleteCart(cartId) {
    return fetch(`${environment.baseUrl}/carts/${cartId}`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

export async function deleteItem(cartId, itemId) {
    const res = await fetch(`${environment.baseUrl}/carts/${cartId}/delete/${itemId}`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    });
    return await res.json();
}