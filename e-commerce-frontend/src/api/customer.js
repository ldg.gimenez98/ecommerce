import { environment } from "../config/environment";

export function getCustomers(){
    return fetch(`${environment.baseUrl}/customers`)
        .then(res => res.json())
}

export function getCustomer(customerId){
    return fetch(`${environment.baseUrl}/customers/${customerId}`)
        .then(res => res.json())
}

export function saveCustomer(customer) {
    return fetch(`${environment.baseUrl}/customers`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "email": customer.email,
            "name": customer.name
        })
    }).then(res => res.json())
}

export function deleteCustomer(customerId) {
    return fetch(`${environment.baseUrl}/customers/${customerId}`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json())
}

export async function customerIsVip(customerId, date){
    const res = await fetch(`${environment.baseUrl}/customers/vip/${customerId}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "date": date
        })
    })
    return await res.json();
}

export async function addOrder(customerId, orderId){
    const res = await fetch(`${environment.baseUrl}/customers/${customerId}/add/${orderId}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    })
    return await res.json();
}

