import React from 'react'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { OrderRow } from '../components/OrderRow';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 13,
    },
  }));

export const OrdersHistory = ({customer}) => {
  const {orders} = customer;

  return (
    <TableContainer component={Paper} style={{margin: "2em auto 2em", maxWidth: "95%"}}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="right">Fecha de compra</StyledTableCell>
            <StyledTableCell align="right">Es VIP</StyledTableCell>
            <StyledTableCell align="right">Es especial</StyledTableCell>
            <StyledTableCell align="right">Importe parcial</StyledTableCell>
            <StyledTableCell align="right">Importe total</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            orders.map((order, key) => (
              <OrderRow key={key} order={order} />
            ))
          }
        </TableBody>
      </Table>
    </TableContainer>
  )
}
