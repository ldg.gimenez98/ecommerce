import React, { useEffect, useState } from 'react'
import { getProducts } from '../api/product';
import { ProductCard } from '../components/ProductCard';

export const ProductsList = ({addProduct}) => {

    const [productsList, setProductsList] = useState([]);

    useEffect(() => {
        let mounted = true;
        getProducts()
        .then(res =>{
            if(mounted){
                setProductsList(res)
            }
        })
        return () => mounted = false;
    }, [])

    return (
        <React.Fragment>
            <div style={{margin: "2em 1em 2em", display: "flex", flexWrap: "wrap", justifyContent: "center"}}>
                { 
                    productsList.map( (product, key) => <ProductCard key={key} product={product} addProduct={(productId) => addProduct(productId)} />)
                }
            </div>
        </React.Fragment>
    )
}
