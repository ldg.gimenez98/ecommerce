import { Alert, Button, Table, TableBody, TableCell, TableContainer, TableRow, Typography } from '@mui/material'
import React from 'react'
import { OrderItemRow } from '../components/OrderItemRow';
import Paper from '@mui/material/Paper';
import { Link } from 'react-router-dom';
import { addOrder, getCustomer } from '../api/customer';
import { createCart, deleteCart } from '../api/cart';


export const Cart = ({customer, setCustomer, order, setOrder, date}) => {
    const {items, totalAmount, finalAmount} = order;


  return (
    order && order.items.length > 0 ?
    <div style={{margin: "2em 1em 2em"}}>
        <Typography style={{fontSize: 24, marginBottom: "1em", fontWeight: "bold"}}>Comprador: {customer.name}</Typography>
        <TableContainer component={Paper} style={{minWidth: "100%"}}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableBody>
                {items.map((item, key) => (
                    <OrderItemRow key={key} item={item} orderId={order.id} setOrder={(newOrder) => setOrder(newOrder)}/>
                ))}
                <TableRow>
                    <TableCell></TableCell>
                    <TableCell align='center'>Importe parcial: </TableCell>
                    <TableCell align='center' style={{textDecoration: "line-through"}}>${totalAmount.toFixed(2)}</TableCell>
                    <TableCell></TableCell>
                </TableRow>
                <TableRow >
                    <TableCell></TableCell>
                    <TableCell align='center'>Importe total: </TableCell>
                    <TableCell align='center'>${finalAmount.toFixed(2)}</TableCell>
                    <TableCell></TableCell>
                </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
        <Link to={"/"} style={{textDecoration: "none"}}>
            <Button style={{margin: "2em 0 auto auto", display: "flex"}} variant="contained" color="success" onClick={() => finishOrder()}>
                Finalizar compra
            </Button>
            <Button style={{margin: "0.5em 0 auto auto", display: "flex"}} variant="contained" color="error" onClick={() => cancelOrder()}>
                Cancelar compra
            </Button>
        </Link>
        
    </div>
    :
    <Alert style={{width: "75%", margin: "5em auto auto"}} severity="info">No tienes productos en tu carrito — <Link to={"/"}>Ver productos</Link></Alert>
  )

  function finishOrder(){
    addOrder(customer.id, order.id);
    createCart(date, customer.id)
    .then(res =>{
        setOrder(res)
    })
    getCustomer(1)
    .then(res =>{
        setCustomer(res)
    })
  }

  function cancelOrder(){
    deleteCart(order.id);
    createCart()
      .then(res =>{
          setOrder(res)
      })
}
}
