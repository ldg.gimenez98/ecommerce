import { useEffect, useState } from 'react';
import './App.css';
import AppBarCust from './components/AppBar';
import { Cart } from './pages/Cart';
import { OrdersHistory } from './pages/OrdersHistory';
import { ProductsList } from './pages/ProductsList';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { getCustomer } from './api/customer';
import { addProductToOrder, createCart } from './api/cart';

function App() {
  const [customer, setCustomer] = useState(null);
  const [order, setOrder] = useState(null);
  const [orderDate, setOrderDate] = useState(new Date())

  useEffect(() => {
    let mounted = true;
    getCustomer(1)
    .then(res =>{
        if(mounted){
          setCustomer(res)
        }
    })
    return () => mounted = false;
  }, [])

  useEffect(() => {
    let mounted = true;
    createCart(orderDate, 1)
    .then(res =>{
        if(mounted){
          setOrder(res)
        }
    })
    return () => mounted = false;
  }, [])

  

  return (
    <>
      <Router>
        <AppBarCust 
          customer={customer} 
          order={order} 
          setOrder={(newOrder) => setOrder(newOrder)}
          date={orderDate} 
          setDate={(date) => setOrderDate(date)}
        />
      
        <Routes>
          <Route path="/" element={ 
              <ProductsList 
                order={order} 
                setOrder={() => setOrder}
                addProduct={(productId) =>addProduct(productId)}
              /> 
            } 
          />

          <Route path="/history" element={ 
              <OrdersHistory customer={customer}/>
            } 
          />

          <Route path="/cart" element={ 
              <Cart 
                customer={customer} 
                setCustomer={() => setCustomer}
                order={order} 
                setOrder={(newOrder) => setOrder(newOrder)}
                date={orderDate}
              />
            } 
          />
        </Routes>
      </Router>
    </>
    
  );

  
  async function addProduct(product){

    addProductToOrder(order.id, product.id)
    .then(res =>{
      setOrder(res)
      console.log(order)
    })
    
  }
}

export default App;
