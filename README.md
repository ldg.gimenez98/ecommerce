# Factor IT - Challenge

***
# # Technologies
A list of technologies used within the project:
* [React](https://es.reactjs.org/): 17.0.2 
* [Bootstrap](https://getbootstrap.com/): 5.1.3
* [Material UI](https://mui.com/): 5.5.2
* [JavaSE](https://spring.io/projects/spring-boot): 11
* [Spring Boot](https://spring.io/projects/spring-boot): 2.6.1

***
# # Installation. 
```
$ git clone https://github.com/ldg.gimenez98/ecommerce.git
$ cd e-commerce-frontend/
$ npm install
$ npm start
```
***

# # Documentation

* [Swaggwer v2](https://swagger.io/): http://localhost:8080/swagger-ui.html

