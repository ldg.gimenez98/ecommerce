package com.ecommerce.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.models.Cart;
import com.ecommerce.app.models.CartItem;
import com.ecommerce.app.repositories.CartItemRepository;
import com.ecommerce.app.repositories.CartRepository;

@Service
public class CartItemService {

	@Autowired
	CartItemRepository cartItemRepository;
	@Autowired
	CartRepository cartRepository;
	
	public CartItem getCartItem(Long id) {
		return cartItemRepository.findById(id).get();
	}
	
	public List<CartItem> getCartItems() {
		return (List<CartItem>) cartItemRepository.findAll();
	}
	
	public CartItem createCartItem(Long cartId, CartItem cartItem) {
		Cart cart = cartRepository.findById(cartId).get();
		cart.addCartItem(cartItem);
		cartRepository.save(cart);

		return cartItem;
	}
	
	public CartItem saveCart(CartItem cartItem) {
		return cartItemRepository.save(cartItem);
	}
}
