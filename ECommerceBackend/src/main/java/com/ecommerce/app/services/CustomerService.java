package com.ecommerce.app.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.models.Cart;
import com.ecommerce.app.models.Customer;
import com.ecommerce.app.repositories.CartRepository;
import com.ecommerce.app.repositories.CustomerRepository;


@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CartRepository cartRepository;
	
	public Customer getCustomer(Long id) {
		return customerRepository.findById(id).get();
	}
	
	public List<Customer> getCustomers() {
		return (List<Customer>) customerRepository.findAll();
	}
	
	public Customer saveCustomer(Customer customer) {
		return customerRepository.save(customer);
	}
	
	public void saveCustomers(List<Customer> customers) {
		customerRepository.saveAll(customers);
	}

	@SuppressWarnings("deprecation")
	public boolean isVip(Long customerId, Date date) {
		Customer customer = customerRepository.findById(customerId).get();
		List<Cart> carts = (List<Cart>) cartRepository.findAllByCustomer(customer);
		
		int month = date.getMonth() - 1;
		
		@SuppressWarnings("static-access")
		Double totalPerMonth = carts.stream()
			.filter(c -> c.getOrderDate().MONTH == month)
			.mapToDouble(c -> c.getTotalAmount())
			.sum();
		
		return (totalPerMonth >= 10000);
	}

	public boolean addOrder(Long customerId, Long orderId) {
		Customer customer = customerRepository.findById(customerId).get();
		
		Cart cart = cartRepository.findById(orderId).get();
		customer.addCart(cart);
		
		saveCustomer(customer);
		
		return true;
	}
}
