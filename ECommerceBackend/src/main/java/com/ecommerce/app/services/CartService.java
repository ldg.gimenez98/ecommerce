package com.ecommerce.app.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.models.Cart;
import com.ecommerce.app.models.CartItem;
import com.ecommerce.app.models.Customer;
import com.ecommerce.app.models.Product;
import com.ecommerce.app.repositories.CartItemRepository;
import com.ecommerce.app.repositories.CartRepository;
import com.ecommerce.app.repositories.CustomerRepository;
import com.ecommerce.app.repositories.ProductRepository;

@Service
public class CartService {
	
	@Autowired
	CartRepository cartRepository;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	CartItemRepository cartItemRepository;
	@Autowired
	CustomerService customerService;
	
	public Cart getCart(Long id) {
		return cartRepository.findById(id).get();
	}
	
	public List<Cart> getCarts() {
		return (List<Cart>) cartRepository.findAll();
	}
	
	public List<Cart> getCartsByCustomer(Long customerId){
		Customer customer = customerRepository.findById(customerId).get();
		
		return (List<Cart>) cartRepository.findAllByCustomer(customer);
	}
	
	public Cart createCart(Date date, Long customerId) {
		Cart cart = new Cart();
	
		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(date);
		
		cart.setOrderDate(dateCalendar);
		cart.setVIP(customerService.isVip(customerId, date));
		
		return saveCart(cart);
	}
	
	public Cart saveCart(Cart cart) {
		return cartRepository.save(cart);
	}

	public Cart addProduct(Long cartId, Long productId) {
		Product product = productRepository.findById(productId).get();
		Cart cart = cartRepository.findById(cartId).get();
		
		CartItem item = containsProduct(cart, product);
		if(item != null) {
			item.setQuantity(item.getQuantity() + 1);
			cart.updateTotalAmount();
		}
		else
			cart.addCartItem(new CartItem(product, 1));
		
		return saveCart(cart);
	}

	public CartItem containsProduct(Cart cart, Product product) {
		for(CartItem item : cart.getItems()) {
			if(item.getProduct().equals(product))
				return item;
		}
		
		return null;
	}

	public Cart deleteItem(Long cartId, Long itemId) {
		CartItem cartItem = cartItemRepository.findById(itemId).get();
		
		Cart cart = cartRepository.findById(cartId).get();
		
		cart.getItems().remove(cartItem);
		
		cart.updateTotalAmount();
		
		return saveCart(cart);
	}

	public Cart updateDate(Long cartId, Date orderDate) {
		Cart cart = cartRepository.findById(cartId).get();

		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(orderDate);
		
		cart.setOrderDate(dateCalendar);
		
		return saveCart(cart);
	}
}
