package com.ecommerce.app.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

import com.ecommerce.app.models.*;
import com.ecommerce.app.services.*;

@Configuration
public class OnBoot implements ApplicationRunner{

	@Autowired
    CustomerService customerService;
	@Autowired
    ProductService productService;
    
	@Override
    public void run(ApplicationArguments args) throws Exception {
       insertCustomers();
    	
    }
	
	public void insertCustomers() {
		List<Customer> customers = new ArrayList<>();
		customers.add(new Customer("Andres Arco", "andresa@gmail.com"));
		customers.add(new Customer("Belen Torre", "belent@gmail.com"));
		customers.add(new Customer("Miguel Rey", "mrey@gmail.com"));
		customers.add(new Customer("Eliana Roldan", "eroldan@gmail.com"));
		customers.add(new Customer("Maria Bustos", "mariab@gmail.com"));
		customers.add(new Customer("Abel Cuevas", "acuevas@gmail.com"));
		customers.add(new Customer("Rocío Gutierrez", "rociog@gmail.com"));
		customers.add(new Customer("Leandro Hierro", "lhierro@gmail.com"));
		
		insertCarts(customers);
		insertCartsItems(customers);
		customerService.saveCustomers(customers);
	}
	
	public void insertCarts(List<Customer> customers) {
		customers.get(0).addCart(new Cart(customers.get(0)));
		customers.get(0).addCart(new Cart(customers.get(0)));
		customers.get(1).addCart(new Cart(customers.get(1)));
		customers.get(4).addCart(new Cart(customers.get(4)));
		customers.get(4).addCart(new Cart(customers.get(4)));
		customers.get(4).addCart(new Cart(customers.get(4)));
		customers.get(5).addCart(new Cart(customers.get(5)));
		customers.get(6).addCart(new Cart(customers.get(6)));
		
	}
	
	public void insertCartsItems(List<Customer> customers) {
		customers.get(0).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(1L), 2));
		customers.get(0).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(4L), 3));
		customers.get(0).getOrders().get(1).addCartItem(new CartItem(productService.getProduct(5L), 2));
		customers.get(1).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(2L), 7));
		customers.get(1).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(5L), 1));
		customers.get(4).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(11L), 5));
		customers.get(4).getOrders().get(1).addCartItem(new CartItem(productService.getProduct(10L), 3));
		customers.get(4).getOrders().get(1).addCartItem(new CartItem(productService.getProduct(11L), 2));
		customers.get(4).getOrders().get(1).addCartItem(new CartItem(productService.getProduct(12L), 4));
		customers.get(4).getOrders().get(2).addCartItem(new CartItem(productService.getProduct(13L), 2));
		customers.get(5).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(1L), 4));
		customers.get(5).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(8L), 1));
		customers.get(5).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(6L), 7));
		customers.get(6).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(9L), 1));
		customers.get(6).getOrders().get(0).addCartItem(new CartItem(productService.getProduct(14L), 3));
	}
}
