package com.ecommerce.app.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class Constants {
	public static final List<Calendar> SPECIAL_DATES = List.of(
				new GregorianCalendar(2022,12,25),
				new GregorianCalendar(2022,12,31),
				new GregorianCalendar(2016,1,1),
				new GregorianCalendar(2016,7,9),
				new GregorianCalendar(2016,5,25),
				new GregorianCalendar(2016,5,25)
			);
}
