package com.ecommerce.app.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.app.models.Cart;
import com.ecommerce.app.models.Customer;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {

	@Query("SELECT c FROM Cart c WHERE c.customer = ?1")
	Iterable<Cart> findAllByCustomer(Customer customer);
}
