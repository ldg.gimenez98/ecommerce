package com.ecommerce.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.app.models.CartItem;

@Repository
public interface CartItemRepository extends CrudRepository<CartItem, Long> {
	
}
