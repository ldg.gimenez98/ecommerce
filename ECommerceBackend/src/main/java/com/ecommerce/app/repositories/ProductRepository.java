package com.ecommerce.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.app.models.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>{

}
