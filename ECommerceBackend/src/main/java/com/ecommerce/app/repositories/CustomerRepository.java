package com.ecommerce.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.app.models.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{

	
}
