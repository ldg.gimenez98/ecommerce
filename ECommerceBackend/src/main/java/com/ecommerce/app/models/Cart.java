package com.ecommerce.app.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

import com.ecommerce.app.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="carts")
public class Cart {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	private Customer customer;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="cart_item_id")
	private List<CartItem> items;
	
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT-3")
	@Column(name="order_date")
	private Calendar orderDate;
	
	private Double totalAmount;
	
	private boolean isVIP;
	
	private boolean specialDate;
	
	private Double finalAmount;

	public Cart() {
		this.items = new ArrayList<>();
		this.orderDate = Calendar.getInstance();
		this.totalAmount = 0d;
		this.finalAmount = 0d;
		this.specialDate = isSpecialDate();
	}
	
	public Cart(Customer customer) {
		super();
		this.customer = customer;
		this.items = new ArrayList<>();
		this.orderDate = Calendar.getInstance();
		this.totalAmount = 0d;
		this.finalAmount = 0d;
		this.specialDate = isSpecialDate();
	}

	public Cart(boolean isVIP) {
		super();
		this.items = new ArrayList<>();
		this.orderDate = Calendar.getInstance();
		this.totalAmount = 0d;
		this.isVIP = isVIP;
		this.finalAmount = 0d;
		this.specialDate = isSpecialDate();
	}
	
	public void addCartItem(CartItem cartItem) {
		
		this.items.add(cartItem);
		
		updateTotalAmount();
	}
	
	public void updateTotalAmount() {
		
		Double newTotal = items.stream().mapToDouble(item -> item.getProduct().getPrize() * item.getQuantity()).sum();
		
		setTotalAmount(newTotal);
		
		updateFinalAmount();
	}
	
	public void updateFinalAmount() {
		int totalQuantity = this.items.stream().mapToInt(item -> item.getQuantity()).sum();
		
		this.finalAmount = this.totalAmount;
		
		if(totalQuantity >= 4 && totalQuantity < 10)
			this.finalAmount = this.totalAmount * 0.75;
		else {
			if(totalQuantity >= 10 && this.isVIP)
				this.finalAmount = this.totalAmount - (500 + cheapestItem());
			if(totalQuantity >= 10 && this.specialDate && !this.isVIP)
				this.finalAmount = this.totalAmount - 300;
			if(totalQuantity >= 10 && !this.isVIP && !this.specialDate)
				this.finalAmount = this.totalAmount - 100;
			
			
		}
	}
	
	private boolean isSpecialDate() {
		
		return Constants.SPECIAL_DATES.contains(orderDate);
	}
	
	public double cheapestItem() {
		return this.items.stream().mapToDouble(item -> item.getProduct().getPrize()).min().getAsDouble();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<CartItem> getItems() {
		return items;
	}

	public void setItems(List<CartItem> items) {
		this.items = items;
	}

	public Calendar getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Calendar orderDate) {
		this.orderDate = orderDate;
		this.specialDate = isSpecialDate();
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public boolean isVIP() {
		return isVIP;
	}

	public void setVIP(boolean isVIP) {
		this.isVIP = isVIP;
	}

	public boolean getSpecialDate() {
		return specialDate;
	}

	public void setSpecialDate(boolean isSpecialDate) {
		this.specialDate = isSpecialDate;
	}

	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}
}
