package com.ecommerce.app.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="customers")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	private String name;
	
	private String email;
	
	@OneToMany(mappedBy="customer", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Cart> orders;
	
	public Customer() {
		
	}

	public Customer(String name, String email) {
		super();
		this.name = name;
		this.email = email;
		this.orders = new ArrayList<>();
	}
	
	public void addCart(Cart newOrder) {
		this.orders.add(newOrder);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Cart> getOrders() {
		return orders;
	}

	public void setOrders(List<Cart> orders) {
		this.orders = orders;
	}
}
