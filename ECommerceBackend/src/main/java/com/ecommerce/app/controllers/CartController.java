package com.ecommerce.app.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.app.models.Cart;
import com.ecommerce.app.services.CartService;

@RestController
@RequestMapping("/carts")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class CartController {

	@Autowired
	CartService cartService;
	
	@GetMapping("/{id}")
	public Cart getCart(@PathVariable("id") Long id){
		return cartService.getCart(id);
	}
	
	@GetMapping()
	public List<Cart> getCarts(){
		return cartService.getCarts();
	}
	
	@GetMapping("/customer/{customerId}")
	public List<Cart> getCartsByCustomer(@PathVariable Long customerId){
		return cartService.getCartsByCustomer(customerId);
	}
	
	@PostMapping("/new/{customerId}")
	public Cart saveCart(@RequestBody Date orderDate, @PathVariable Long customerId) {
		return cartService.createCart(orderDate, customerId);
	}
	
	@PostMapping("/{cartId}/add/{productId}")
	public Cart addProduct(@PathVariable Long cartId, @PathVariable Long productId) {
		return cartService.addProduct(cartId, productId);
	}
	
	@PutMapping()
	public Cart updateCart(@RequestBody Cart cart) {
		return cartService.saveCart(cart);
	}
	
	@PutMapping("/{cartId}")
	public Cart updateDate(@PathVariable Long cartId, @RequestBody Date orderDate) {
		return cartService.updateDate(cartId, orderDate);
	}
	
	@DeleteMapping("/{cartId}/delete/{itemId}")
	public Cart deleteItem(@PathVariable Long cartId, @PathVariable Long itemId) {
		return cartService.deleteItem(cartId, itemId);
	}
}
