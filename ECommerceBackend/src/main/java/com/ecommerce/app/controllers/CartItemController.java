package com.ecommerce.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.app.models.CartItem;
import com.ecommerce.app.services.CartItemService;

@RestController
@RequestMapping("/cartsItems")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class CartItemController {
	
	@Autowired
	CartItemService cartItemService;
	
	@GetMapping("/{id}")
	public CartItem getCartItem(@PathVariable("id") Long id){
		return cartItemService.getCartItem(id);
	}
	
	@GetMapping()
	public List<CartItem> getCustomers(){
		return cartItemService.getCartItems();
	}
	
	@PostMapping("/{cartId}")
	public CartItem createCartItem(@PathVariable Long cartId, @RequestBody CartItem cartItem) {
		return cartItemService.createCartItem(cartId, cartItem);
	}


}
