package com.ecommerce.app.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.app.models.Customer;
import com.ecommerce.app.services.CustomerService;

@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/{id}")
	public Customer getCustomer(@PathVariable("id") Long id){
		return customerService.getCustomer(id);
	}
	
	@GetMapping()
	public List<Customer> getCustomers(){
		return customerService.getCustomers();
	}
	
	@PostMapping("/vip/{customerId}")
	public boolean isVip(@PathVariable Long customerId, @RequestBody Date date){
		return customerService.isVip(customerId, date);
	}
	
	@PostMapping("/{customerId}/add/{orderId}")
	public boolean addOrder(@PathVariable Long customerId, @PathVariable Long orderId){
		return customerService.addOrder(customerId, orderId);
	}
	
	@PutMapping()
	public Customer saveCustomer(@RequestBody Customer customer) {
		return customerService.saveCustomer(customer);
	}

}
